// 解决clike300毫秒延迟问题
FastClick.attach(document.body);
//音乐播放器
const musicModle = (function () {
  const playBtn = document.querySelector('.play_bnt'),
    wrapperBox = document.querySelector('.wrapper'),
    currentBox = document.querySelector('.current'),
    durationBox = document.querySelector('.duration'),
    alreadyBox = document.querySelector('.already'),
    audioBox = document.querySelector('#audioBox');
  let wrapperList = [];
  timer = null;
  n = 0;
  const queryData = function querDate() {
    return new Promise(resolve => {
      let xhr = new XMLHttpRequest;
      xhr.open('GET', './json/lyric.json')
      xhr.onreadystatechange = () => {
        let {
          readyState,
          status,
          responseText
        } = xhr
        if (readyState === 4 && status === 200) {
          let data = JSON.parse(responseText)
          resolve(data);
        }
      };
      xhr.send();

    })

  }


    /* 歌词绑定 */
    const binding = function binding(lyric) {
      // 歌词的格式化处理
      let arr = [];
      lyric = lyric.replace(/(\d+);/g, (val, $1) => {
          let char = val;
          switch (+$1) {
              case 32:
                  char = " ";
                  break;
              case 40:
                  char = "(";
                  break;
              case 41:
                  char = ")";
                  break;
              case 45:
                  char = "-";
                  break;
          }
          return char;
      });
      lyric.replace(
          /\[(\d+):(\d+).(?:\d+)\]([^/\n]+)/g, (_, $1, $2, $3) => {
              arr.push({
                  minutes: $1,
                  seconds: $2,
                  text: $3
              });
          });
    let str = ``;
    arr.forEach(item => {
      let {
        minutes,
        seconds,
        text
      } = item;
      str += `<P minutes="${minutes}" seconds="${seconds}">
  ${text}
  </P>`;
    });
    wrapperBox.innerHTML = str;
    wrapperList = Array.from(wrapperBox.querySelectorAll('p'));
  };
  // 其他操作
  const handler = function handler() {
    //处理总时间
    let duration = audioBox.duration;
    if (duration) {
      let {
        minutes,
        seconds
      } = formatTime(duration);
      durationBox.innerHTML = `${minutes}:${seconds}`;
    }

    //播放按钮时间绑定
    playBtn.onclick = function () {
      if (audioBox.paused) {
        //当前暂停
        audioBox.play();
        this.classList.add('move');
        $sub.emit('playing');
        timer = setInterval(() => {
          $sub.emit('playing');
        }, 1000);
        return
      }
      //当前播放
      audioBox.pause();
      this.classList.remove('move');
      clearInterval(timer)
    }
  }
  // 时间格式化
  const formatTime = function formatTime(time) {
    let minutes = Math.floor(time / 60),
      seconds = Math.ceil(time - minutes * 60);
    if (minutes < 10) minutes = '0' + minutes;
    if (seconds < 10) seconds = '0' + seconds;
    return {
      minutes,
      seconds
    };
  };
  //音乐播放和暂停处理
  $sub.on('playing', () => {
    //控制歌词处理
    let {
      minutes,
      seconds
    } = formatTime(audioBox.currentTime);
    //获取当前已经播放时间的分钟和秒
    let arr = wrapperList.filter(item => { //和所有p标签进行匹配，把相同时间的p标签获取，放在arr数组中
      let minutesAttr = item.getAttribute('minutes')
      let secondsAttr = item.getAttribute('seconds')
      return minutesAttr === String(minutes) && secondsAttr === String(seconds);
    });
    if (arr.length === 0) return; //如果没有匹配的则不需要任何的处理
    wrapperList.forEach(item => {
      if (arr.includes(item)) { //如果有匹配的，则把匹配的设置选中样式，其余的移除选中样式
        item.className = "active";
        return;
      }
      item.className = "";
    });
    //让歌词移动
    n += arr.length;
    if (n >= 4) {
      let pH = wrapperList[0].offsetHeight; //一个p标签的高度
      wrapperBox.style.transform = `translateY(${-((n-3)*pH)}px)`;
    }
  });

  $sub.on('playing', () => {
    //控制进度条的处理
    let currentTime = audioBox.currentTime,
      duration = audioBox.duration;
    if (!currentTime || !duration) return;
    //进度条
    let ratio = (currentTime / duration).toFixed(2)
    if (ratio > 1) ratio = 1;
    alreadyBox.style.width = `${ratio*100}%`;
    //时间处理
    let currentObj = formatTime(currentTime),
      durationObj = formatTime(duration);
    currentBox.innerHTML = `${currentObj.minutes}:${currentObj.seconds}`;
    durationBox.innerHTML = `${durationObj.minutes}:${durationObj.seconds}`;
    //播放完毕
    if (currentTime >= duration) $sub.emit('playend');
  });


  $sub.on('playend', () => {
    //播放完毕处理
    playBtn.classList.remove('move');
    clearInterval(timer);
    n = 0;
    currentBox.innerHTML = '00:00';
    alreadyBox.style.width = `0`;
    wrapperList.forEach(item => item.className = '')
    wrapperBox.style.transform = `translateY(0)`
  });
  return {
    async init() {
      let {
        lyric
      } = await queryData()
      // console.log(lyric);
      binding(lyric);
      handler()
    }
  }
})();
musicModle.init();